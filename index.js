/**                              Meteor.Js

Meteor.Js Web ve Mobil uygulamalar geliştirmek için JavaScript platformudur.

MeteorJs ile server ile client tek bir uygulama içerisinde geliştirilir.

Uygulama geliştirme hızını önemli ölçüde arttırır.

MeteorJs ile gerçek zamanlı uygulama yapmak çok basittir.

MeteorJs kendi içinde MongoDb barındırır.

MeteorJs içinde barındırdığı MongoDb ile gerçek zamanlı bağlantı bir bağlantı kurar. 

*/